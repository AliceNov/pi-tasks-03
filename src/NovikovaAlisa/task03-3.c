#define N 256
#include <stdio.h>
#include <string.h>


int main()
{
	char line[N];
	int count[N] = { 0 };
	int i = 0;

	printf("Enter a line:\n");
	fgets(line, N, stdin);
	line[strlen(line) - 1] = 0;

	do{
		count[line[i++]]++;
	}while (line[i]);

	for (i = 0; i<N; i++)
	{
		if (count[i] != 0)
			printf("%c = %d \n", i, count[i]);
	}
	return 0;
}